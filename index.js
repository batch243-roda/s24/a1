const getCube = (num) => {
  const cube = Math.pow(num, 3);
  console.log(`The cube of ${num} is ${cube}`);
};
getCube(2);

const address = ["A. Bonifacio St.", "Baybay City", "Leyte"];

const [street, muncipality, province] = address;
console.log(`I live at ${street}, ${muncipality}, ${province}`);

const Animal = {
  animalName: "Lolong",
  species: "Crocodile",
  weight: 1075,
  measurement: {
    ft: 20,
    inch: 3,
  },
};

const {
  animalName,
  species,
  weight,
  measurement: { ft },
  measurement: { inch },
} = Animal;
console.log(
  `${animalName} was a saltwater ${species}. He weighed at ${weight} kgs with a measurement of ${ft} ft ${inch} in.`
);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

const reduceNumber = numbers.reduce((total, num) => (total += num));
console.log(reduceNumber);

class Dog {
  constructor(name, age, breed) {
    this.dogName = name;
    this.dogAge = age;
    this.dogBreed = breed;
  }
}

const frankie = new Dog("Frankie", 5, "Miniature Dachsund");
console.log(frankie);
